//st-spi.ino
#include <SPI.h>

#define CS 10
#define RST 9
#define SET 8

#define byteLength 8
#define numDrivers 12
#define numPorts 8
#define ton 400
#define toff 50

int rd[numDrivers*numPorts];
int rd1[8];
int rd2[8];
int rd3[8];
int rd4[8];
int rd5[8];
int rd6[8];
int rd7[8];
int rd8[8];
int rd9[8];
int rd10[8];
int rd11[8];
int rd12[8];

byte b1;
byte b2;
byte b3;
byte b4;
byte b5;
byte b6;
byte b7;
byte b8;
byte b9;
byte b10;
byte b11;
byte b12;

byte nextByte;

void setup() {
  pinMode(CS, OUTPUT);
  pinMode(RST, OUTPUT);
  pinMode(SET, OUTPUT);
  digitalWrite(CS, HIGH);
  digitalWrite(RST, HIGH);
  digitalWrite(SET, HIGH);
  
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);
  
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  SPI.setDataMode(SPI_MODE0);
  SPI.begin();
  
  Serial.begin(9600);
  delay(10);
  Serial.write((byte)0);
  
//  initRelayDriver();
  resetArray();
  forceResetArray();
//  transferArray();
}

void loop() {
  if (Serial.available()) {
    digitalWrite(5, HIGH);
    
    nextByte = Serial.read();
    
    switch(nextByte) {
      case 0:
      {
        Serial.write((byte)0);
        break;
      }


      case 1:
      {
        Serial.write((byte)1);

        while (!Serial.available()) {
          demo();
        }
        break;
      }


      case 2:
      {
        Serial.write((byte)2);

        Serial.flush();
        delay(500);
        while (!Serial.available()); //wait until data available
        
        delay(1);
        int pad = Serial.read();

        delay(1); //Necessary delay between reading bytes
        
        if (pad >= 0 && pad <= 95) {  //valid pad range?
          int value = Serial.read();
//          if (value >= 0 && value <= 1) { //valid ON/OFF value?
//            setIndex(pad, value);
//            Serial.write(pad);            //
//            Serial.write(value);
            switch (value) {
              case 0:
                turnOffPin(pad, 1);
                Serial.write((byte)pad);            
                Serial.write((byte)value);
                break;
              case 1:
                turnOnPin(pad, 1);
                Serial.write((byte)pad);            
                Serial.write((byte)value);
                break;
              default:
                for (int i=0;i<100000;i++) {
                  togglePin(pad, 400, 50);
                }
                Serial.write((byte)pad);            
                Serial.write((byte)value);           
                break;
          }
        }

        delay(10);
        Serial.write((byte)0);
        break;
      }

      default:
      {
        //Else: ask for Menu on python side
        Serial.write((byte)0); 
        break;
      }
    }
    
  }
}

void demo() {
  //blink LED at beginning of loop
  for (int i=0; i<10; i++) {
    digitalWrite(7, HIGH);
    delay(50);
    digitalWrite(7, LOW);
    delay(50);
  }
  
//  delay(2000);
  
  int t1 = 200;
  int t2 = 100;
  
//  for (int i=0; i<96; i++) {
//    togglePin(i, t1, t2);
//  }
  
  for (int i=0; i<96; i++) {
    togglePin(i, t1, t2);
  }
  
//  forceResetArray();
//  transferArray();
//  int x = 16;
//  togglePin(x, t1, t2);
//  x = 31;
//  togglePin(x, t1, t2);
//  x = 47;
//  togglePin(x, t1, t2);
//  x = 64;
//  togglePin(x, t1, t2);
//  x = 79;
//  togglePin(x, t1, t2);
//  x = 95;
//  togglePin(x, t1, t2);
//
//  int x = 16;
//  togglePin(x, t1, t2);
//  x = 17;
//  togglePin(x, t1, t2);
//  x = 11;
//  togglePin(x, t1, t2);
//  x = 12;
//  togglePin(x, t1, t2);
//
//  int p1 = 16;
//  int p2 = 17;
//  int p3 = 11;
//  int p4 = 12;
//  turnOnPin(p1, 0);
//  turnOnPin(p2, 0);
//  turnOnPin(p3, 0);
//  turnOnPin(p4, 0);
//  delay(t1);
//  turnOffPin(p1, 0);
//  turnOffPin(p2, 0);
//  turnOffPin(p3, 0);
//  turnOffPin(p4, 0);
//  delay(t2);
}

byte arrayToByte(int arr[], int len) {
  // Convert -1 to 0 and pack the array into a byte
  int i;
  byte result = 0;

  for(i=len-1; i>=0; i--){
    if(arr[i] == -1){
      result &= ~(0 << i);
    } else {
      result |= (1 << i);
    }
  }
  return result;
}

void setIndex(int index, int value) {
  rd[index] = value;
}

int getIndex(int index) {
  return rd[index];
}

void resetArray() {
//  memset(rd, -1, numDrivers*numPorts*sizeof(int));
  memset(rd, -1, 96*sizeof(int));
}

void forceResetArray() {
  for (int i=0; i<96; i++) {
    setIndex(i, -1);
//    transferArray();
  }
  transferArray(); 
}

void turnOnPin(int index, int t) {
  setIndex(index, 1);
  transferArray();
  digitalWrite(7, HIGH);
  delay(t);
}

void turnOffPin(int index, int t) {
  setIndex(index, -1);
  transferArray();
  digitalWrite(7, LOW);
  delay(t);
}

void togglePin(int index, int t1, int t2) {
  turnOnPin(index, t1);
  turnOffPin(index, t2);
}

void transferArray() {
  rd1[0]=rd[0];  rd1[1]=rd[1];  rd1[2]=rd[2];  rd1[3]=rd[3];  rd1[4]=rd[4];  rd1[5]=rd[5];  rd1[6]=rd[6];  rd1[7]=rd[7];
  rd2[0]=rd[8];  rd2[1]=rd[9];  rd2[2]=rd[10]; rd2[3]=rd[11]; rd2[4]=rd[12]; rd2[5]=rd[13]; rd2[6]=rd[14]; rd2[7]=rd[15];
  rd3[0]=rd[16]; rd3[1]=rd[17]; rd3[2]=rd[18]; rd3[3]=rd[19]; rd3[4]=rd[20]; rd3[5]=rd[21]; rd3[6]=rd[22]; rd3[7]=rd[23];
  rd4[0]=rd[24]; rd4[1]=rd[25]; rd4[2]=rd[26]; rd4[3]=rd[27]; rd4[4]=rd[28]; rd4[5]=rd[29]; rd4[6]=rd[30]; rd4[7]=rd[31];
  rd5[0]=rd[32]; rd5[1]=rd[33]; rd5[2]=rd[34]; rd5[3]=rd[35]; rd5[4]=rd[36]; rd5[5]=rd[37]; rd5[6]=rd[38]; rd5[7]=rd[39];
  rd6[0]=rd[40]; rd6[1]=rd[41]; rd6[2]=rd[42]; rd6[3]=rd[43]; rd6[4]=rd[44]; rd6[5]=rd[45]; rd6[6]=rd[46]; rd6[7]=rd[47];
  rd7[0]=rd[48]; rd7[1]=rd[49]; rd7[2]=rd[50]; rd7[3]=rd[51]; rd7[4]=rd[52]; rd7[5]=rd[53]; rd7[6]=rd[54]; rd7[7]=rd[55];
  rd8[0]=rd[56]; rd8[1]=rd[57]; rd8[2]=rd[58]; rd8[3]=rd[59]; rd8[4]=rd[60]; rd8[5]=rd[61]; rd8[6]=rd[62]; rd8[7]=rd[63];
  rd9[0]=rd[64]; rd9[1]=rd[65]; rd9[2]=rd[66]; rd9[3]=rd[67]; rd9[4]=rd[68]; rd9[5]=rd[69]; rd9[6]=rd[70]; rd9[7]=rd[71];
  rd10[0]=rd[72]; rd10[1]=rd[73]; rd10[2]=rd[74]; rd10[3]=rd[75]; rd10[4]=rd[76]; rd10[5]=rd[77]; rd10[6]=rd[78]; rd10[7]=rd[79];
  rd11[0]=rd[80]; rd11[1]=rd[81]; rd11[2]=rd[82]; rd11[3]=rd[83]; rd11[4]=rd[84]; rd11[5]=rd[85]; rd11[6]=rd[86]; rd11[7]=rd[87];
  rd12[0]=rd[88]; rd12[1]=rd[89]; rd12[2]=rd[90]; rd12[3]=rd[91]; rd12[4]=rd[92]; rd12[5]=rd[93]; rd12[6]=rd[94]; rd12[7]=rd[95];

  b1 = arrayToByte(rd1, 8);
  b2 = arrayToByte(rd2, 8);
  b3 = arrayToByte(rd3, 8);
  b4 = arrayToByte(rd4, 8);
  b5 = arrayToByte(rd5, 8);
  b6 = arrayToByte(rd6, 8);
  b7 = arrayToByte(rd7, 8);
  b8 = arrayToByte(rd8, 8);
  b9 = arrayToByte(rd9, 8);
  b10 = arrayToByte(rd10, 8);
  b11 = arrayToByte(rd11, 8);
  b12 = arrayToByte(rd12, 8);

  digitalWrite(CS, LOW);
  SPI.transfer(b12);
  SPI.transfer(b11);
  SPI.transfer(b10);
  SPI.transfer(b9);
  SPI.transfer(b8);
  SPI.transfer(b7);
  SPI.transfer(b6);
  SPI.transfer(b5);
  SPI.transfer(b4);
  SPI.transfer(b3);
  SPI.transfer(b2);
  SPI.transfer(b1);
  digitalWrite(CS, HIGH);
}

void initRelayDriver() {
//  digitalWrite(RST, LOW);
//  delay(100);
  digitalWrite(RST, HIGH);
  digitalWrite(SET, HIGH);
}
