//st-spi.ino
#include <SPI.h>

#define CS 10
#define RST 9
#define SET 8
#define LED 7

#define byteLength 8
#define numDrivers 12 //Number of MAX4820 ICs on vertical connector board
#define numPorts 8 //Number of outputs per relay driver

int rd[numDrivers*numPorts];
int rd1[8];
int rd2[8];
int rd3[8];
int rd4[8];
int rd5[8];
int rd6[8];
int rd7[8];
int rd8[8];
int rd9[8];
int rd10[8];
int rd11[8];
int rd12[8];

byte b1;
byte b2;
byte b3;
byte b4;
byte b5;
byte b6;
byte b7;
byte b8;
byte b9;
byte b10;
byte b11;
byte b12;



byte nextByte;

void setup() {
  pinMode(CS, OUTPUT); //Chip Select for SPI
  pinMode(RST, OUTPUT); //If RESET goes low, all relays turn off
  pinMode(SET, OUTPUT); //If SET goes low, all relays turn on
  digitalWrite(CS, HIGH);
  digitalWrite(RST, HIGH);
  digitalWrite(SET, HIGH);
  
  //LED indicator
  pinMode(6, OUTPUT);
  pinMode(LED, OUTPUT);
  digitalWrite(6, LOW);
  digitalWrite(LED, LOW);
  
  //"Power Good" signal
  //Turns on relay which gives power to relay driver chips
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);
  
  //Standard SPI stuff...
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  SPI.setDataMode(SPI_MODE0);
  SPI.begin();
  
  Serial.begin(9600);
  delay(10);

  //Write a byte "0" over Serial port to the Python script.
  //Whenever the Python script receives a 0, it will show the Demo menu
  Serial.write((byte)0);
  
  //For these two functions below...
  //I never actually did narrow down what is necessary to not get the relay 
  //drivers to "freak out" at startup. Sometimes forceResetArray() is needed,
  //sometimes it is not. Sometimes it will still freak out. This is why I
  //added the "Power Good" signal on Pin 5.
  resetArray(); //memset
  forceResetArray(); //Clock out 0's over SPI
}

void loop() {
  //The Python script should be sitting at the menu right now since we sent
  //a "0" over Serial in the setup(). When we have some Serial data,
  //determine what the Arduino needs to do.

  if (Serial.available()) {
    //Enable "Power Good" signal now that we're in operation
    digitalWrite(5, HIGH);
    
    nextByte = Serial.read();
    
    switch(nextByte) {
      case 0:
      {
        //Tell Python script to go to Menu
        Serial.write((byte)0);
        break;
      }

      case 1:
      {
        //Confirm with Python script that the request for a demo was heard
        Serial.write((byte)1);

        Serial.flush();
        delay(500);

        //Now that Python script knows we confirmed, wait for Python script
        //to tell us what demo to run
        while(!Serial.available());

        delay(1);

        int demo = Serial.read();

        if (demo >= 1 && demo <= 2) {  //Need to update this if you add demos
          delay(0.1);
          switch (demo) {
            case 1:
              Serial.write((byte)1);
              while(!Serial.available()) { //Keep iterating unless cancel
                singlePadMappedDemo();
              }
              break;

            case 2:
              Serial.write((byte)2);
              while(!Serial.available()) { //Keep iterating unless cancel
                //demo2()
              }
              break;

            default:
              Serial.write((byte)0);
              break;
          }
        }
        else {
          Serial.write((byte)0);
        }
        break;
      }

      case 2:
      {
        Serial.write((byte)2);

        Serial.flush();
        delay(100);

        //Wait for the "Pad" and "Value" information 
        while (!Serial.available()); //Hang until there is Serial data
        delay(100); //Necessary delay so that all 3 bytes we are expecting
        //are there already. Implement a more elegant solution later.

        int xcoord = Serial.read();
        int ycoord = Serial.read();
        int value = Serial.read();
        int pad = mappedPad(xcoord, ycoord);
        
        if (value == 0) {
          Serial.write((byte)pad);
          delay(1);
          Serial.write((byte)value);
          delay(1);
          turnOffPin(pad, 1);
        }
        else if (value == 1) {
          Serial.write((byte)pad);
          delay(1);
          Serial.write((byte)value);
          delay(1);
          turnOnPin(pad, 1);
        }
        else if (value == 2) {
          Serial.write((byte)pad);
          delay(1);
          Serial.write((byte)value);
          delay(1);
          for (int i=0; i<30; i++) {
            togglePin(pad, 400, 50);
          }
        }
        else {
          Serial.write((byte)pad);
          delay(1);
          Serial.write((byte)value);
          delay(1);
        }
        // }
        // else {
        //   Serial.write((byte)100);
        //   delay(0.1)
        //   Serial.write((byte)100);
        //   delay(0.1)
        // }

        delay(1);
        // Go back to single pad control
        Serial.write((byte)0);
        break;
      }

      default:
      {
        //Ask for menu in Python script
        Serial.write((byte)0); 
        break;
      }
    }
  }
}

void singlePadCycleDemo() {
  for (int i=0; i<10; i++) { //Blink LED at beginning of loop
    digitalWrite(LED, HIGH);
    delay(50);
    digitalWrite(LED, LOW);
    delay(50);
  }
  delay(5000); //Wait 5000 second after blinking
  
  //Time on, off variables
  int ton = 200;
  int toff = 100;
  
  //Go through all 96 pins
  for (int i=0; i<96; i++) {
    togglePin(i, ton, toff);
  } 
}

void singlePadMappedDemo() {
  for (int i=0; i<10; i++) { //Blink LED at beginning of loop
    digitalWrite(LED, HIGH);
    delay(50);
    digitalWrite(LED, LOW);
    delay(50);
  }
  
  int ton = 1000;
  int toff = 1;
  
  for (int i=0;i<8;i++) {
    for (int j=0;j<12;j++) {
      int pad = mappedPad(i,j);
      togglePin(pad, ton, toff);   
    }
  }
}

byte arrayToByte(int arr[], int len) {
  // Convert -1 to 0 and pack the array into a byte
  int i;
  byte result = 0;

  for(i=len-1; i>=0; i--){
    if(arr[i] == -1){
      result &= ~(0 << i);
    } else {
      result |= (1 << i);
    }
  }
  return result;
}

void setIndex(int index, int value) {
  rd[index] = value;
}

int getIndex(int index) {
  return rd[index];
}

void resetArray() {
//  memset(rd, -1, numDrivers*numPorts*sizeof(int));
  memset(rd, -1, 96*sizeof(int));
}

void forceResetArray() {
  for (int i=0; i<96; i++) {
    setIndex(i, -1);
  }
  transferArray(); 
}

void turnOnPin(int index, int t) {
  setIndex(index, 1);
  transferArray();
  delay(t);
  flashLED(1);
}

void turnOffPin(int index, int t) {
  setIndex(index, -1);
  transferArray();
  delay(t);
}

void togglePin(int index, int t1, int t2) {
  turnOnPin(index, t1);
  digitalWrite(7, HIGH);
  turnOffPin(index, t2);
  digitalWrite(7, LOW);
}

void transferArray() {
  rd1[0]=rd[0];  rd1[1]=rd[1];  rd1[2]=rd[2];  rd1[3]=rd[3];  rd1[4]=rd[4];  rd1[5]=rd[5];  rd1[6]=rd[6];  rd1[7]=rd[7];
  rd2[0]=rd[8];  rd2[1]=rd[9];  rd2[2]=rd[10]; rd2[3]=rd[11]; rd2[4]=rd[12]; rd2[5]=rd[13]; rd2[6]=rd[14]; rd2[7]=rd[15];
  rd3[0]=rd[16]; rd3[1]=rd[17]; rd3[2]=rd[18]; rd3[3]=rd[19]; rd3[4]=rd[20]; rd3[5]=rd[21]; rd3[6]=rd[22]; rd3[7]=rd[23];
  rd4[0]=rd[24]; rd4[1]=rd[25]; rd4[2]=rd[26]; rd4[3]=rd[27]; rd4[4]=rd[28]; rd4[5]=rd[29]; rd4[6]=rd[30]; rd4[7]=rd[31];
  rd5[0]=rd[32]; rd5[1]=rd[33]; rd5[2]=rd[34]; rd5[3]=rd[35]; rd5[4]=rd[36]; rd5[5]=rd[37]; rd5[6]=rd[38]; rd5[7]=rd[39];
  rd6[0]=rd[40]; rd6[1]=rd[41]; rd6[2]=rd[42]; rd6[3]=rd[43]; rd6[4]=rd[44]; rd6[5]=rd[45]; rd6[6]=rd[46]; rd6[7]=rd[47];
  rd7[0]=rd[48]; rd7[1]=rd[49]; rd7[2]=rd[50]; rd7[3]=rd[51]; rd7[4]=rd[52]; rd7[5]=rd[53]; rd7[6]=rd[54]; rd7[7]=rd[55];
  rd8[0]=rd[56]; rd8[1]=rd[57]; rd8[2]=rd[58]; rd8[3]=rd[59]; rd8[4]=rd[60]; rd8[5]=rd[61]; rd8[6]=rd[62]; rd8[7]=rd[63];
  rd9[0]=rd[64]; rd9[1]=rd[65]; rd9[2]=rd[66]; rd9[3]=rd[67]; rd9[4]=rd[68]; rd9[5]=rd[69]; rd9[6]=rd[70]; rd9[7]=rd[71];
  rd10[0]=rd[72]; rd10[1]=rd[73]; rd10[2]=rd[74]; rd10[3]=rd[75]; rd10[4]=rd[76]; rd10[5]=rd[77]; rd10[6]=rd[78]; rd10[7]=rd[79];
  rd11[0]=rd[80]; rd11[1]=rd[81]; rd11[2]=rd[82]; rd11[3]=rd[83]; rd11[4]=rd[84]; rd11[5]=rd[85]; rd11[6]=rd[86]; rd11[7]=rd[87];
  rd12[0]=rd[88]; rd12[1]=rd[89]; rd12[2]=rd[90]; rd12[3]=rd[91]; rd12[4]=rd[92]; rd12[5]=rd[93]; rd12[6]=rd[94]; rd12[7]=rd[95];

  b1 = arrayToByte(rd1, 8);
  b2 = arrayToByte(rd2, 8);
  b3 = arrayToByte(rd3, 8);
  b4 = arrayToByte(rd4, 8);
  b5 = arrayToByte(rd5, 8);
  b6 = arrayToByte(rd6, 8);
  b7 = arrayToByte(rd7, 8);
  b8 = arrayToByte(rd8, 8);
  b9 = arrayToByte(rd9, 8);
  b10 = arrayToByte(rd10, 8);
  b11 = arrayToByte(rd11, 8);
  b12 = arrayToByte(rd12, 8);

  digitalWrite(CS, LOW);
  SPI.transfer(b12);
  SPI.transfer(b11);
  SPI.transfer(b10);
  SPI.transfer(b9);
  SPI.transfer(b8);
  SPI.transfer(b7);
  SPI.transfer(b6);
  SPI.transfer(b5);
  SPI.transfer(b4);
  SPI.transfer(b3);
  SPI.transfer(b2);
  SPI.transfer(b1);
  digitalWrite(CS, HIGH);
}

void flashLED(int num) {
  for (int i=0; i<num; i++) {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }
}

int mappedPad(int x, int y) {
  int staticmap[12][8] = {
     /*   0,  1,  2,  3,  4,  5,  6,  7*/
  /*0*/  {31, 36, 41, 46,    49, 54, 59, 64},
  /*1*/  {30, 35, 40, 45,    50, 55, 60, 65},
  /*2*/  {29, 34, 39, 44,    51, 56, 61, 66},
  /*3*/  {28, 33, 38, 43,    52, 57, 62, 67},
  /*4*/  {27, 32, 37, 47,    48, 58, 63, 68},
  /*5*/  {24, 26, 25, 42,    53, 70, 69, 71},
  
  /*6*/  {23, 21, 22,  5,    90, 73, 74, 72},
  /*7*/  {20, 15, 10,  0,    95, 85, 80, 75},
  /*8*/  {19, 14,  9,  4,    91, 86, 81, 76},
  /*9*/  {18, 13,  8,  3,    92, 87, 82, 77},
  /*10*/ {17, 12,  7,  2,    93, 88, 83, 78},
  /*11*/ {16, 11,  6,  1,    94, 89, 84, 79}}; 


//   int staticmap[12][8] = {
//   /*   0,  1,  2,  3,  4,  5,  6,  7*/
//       {31, 36, 41, 46, 49, 54, 59, 64},
// /*1*/ {30, 35, 40, 45, 50, 55, 60, 65},
// /*2*/ {29, 34, 39, 44, 51, 56, 61, 66},
// /*3*/ {28, 33, 38, 43, 52, 57, 62, 67},
// /*4*/ {27, 32, 37, 47, 48, 58, 63, 68},
// /*5*/ {24, 26, 25, 42, 53, 70, 71, 69},
// /*6*/ {23, 21, 22,  5, 90, 84, 78, 72},
// /*7*/ {20, 15, 10,  0, 95, 85, 79, 73},
// /*8*/ {19, 14,  9,  4, 91, 86, 80, 74},
// /*9*/ {18, 13,  8,  3, 92, 87, 81, 75},
// /*10*/{17, 12,  7,  2, 93, 88, 82, 76},
// /*11*/{16, 11,  6,  1, 94, 89, 83, 77}
// };
  return staticmap[y][x];
}
